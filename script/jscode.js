function createNewUser() {

  let newFirstName = prompt('Write your name pls', 'Joe');
  let newLastName = prompt('Write your Surname pls', 'Satriani');
  let dd = prompt('Enter the DAY of your Birthday pls from 1 to 31', '16');
  let mm = prompt('Enter the MONTH of your Birthday pls from 01 to 12', '7') - 1;
  let yyyy = prompt('Enter the YEAR of your Birthday pls!', '1990');
  let newUser = {
    firstName: newFirstName,
    lastName: newLastName,
    birthDay: `${dd}.${(mm + 1 > 9 ? mm + 1 : '0' + (mm + 1))}.${yyyy}`,
    getLogin: function () {
      return (this.firstName[0] + this.lastName).toLowerCase()
    },
    setFirstName: function (value) {
      Object.defineProperty(this, 'firstName', { writable: true });
      this.firstName = value;
      Object.defineProperty(this, 'firstName', { writable: false });
    },
    setLastName: function (value) {
      Object.defineProperty(this, 'lastName', { writable: true });
      this.lastName = value;
      Object.defineProperty(this, 'lastName', { writable: false });
    },
    getAge: function () {
      return Math.floor((Date.now() - new Date(yyyy, mm, dd)) / (3600 * 24 * 365.25 * 1000))
    },
    getPassword: function () {
      return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + yyyy)
    }
  }
  Object.defineProperty(newUser, 'firstName', { writable: false });
  Object.defineProperty(newUser, 'lastName', { writable: false });
  return newUser;
};

let user = new createNewUser()

console.log('user.getLogin()', user.getLogin());
console.log('user.getAge()', user.getAge());
console.log('user.getPassword()', user.getPassword());

